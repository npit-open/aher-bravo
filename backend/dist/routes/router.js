"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const userController_1 = require("../controllers/userController");
const express_1 = __importDefault(require("express"));
const router = express_1.default.Router();
// User routes -->
router.get("/", userController_1.GetVersion);
router.post("/api/Users/Save", userController_1.SaveUser);
router.get("/api/Users/GetAll", userController_1.GetAllUsers);
router.get("/api/Users/GetUser/:id", userController_1.GetSelectedUser);
router.post("/api/Users/Update/:id", userController_1.UpdateUser);
router.get("/api/Users/Delete/:id", userController_1.DeleteUser);
exports.default = router;
//# sourceMappingURL=router.js.map