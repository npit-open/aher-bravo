"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const users_1 = __importDefault(require("../models/users"));
exports.GetVersion = (req, res, next) => {
    res.status(200).json({
        success: true,
        version: "1.0.0"
    });
};
/* Save User */
exports.SaveUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    const Name = req.body.name;
    const Email = req.body.email;
    const Address = req.body.address;
    const Dob = req.body.dob;
    const userDetails = new users_1.default({
        name: Name,
        email: Email,
        address: Address,
        dob: Dob
    });
    try {
        const userRes = yield users_1.default.find({ email: Email });
        if (userRes[0] === undefined) {
            const response = yield userDetails.save();
            res.status(200).json({
                success: true,
                message: "User inserted successfully",
                data: response
            });
        }
        else {
            res.status(201).json({
                success: false,
                message: "Email already exists!"
            });
        }
    }
    catch (err) {
        console.log("Error in insertion of user: " + err.message);
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});
/* Get All Users */
exports.GetAllUsers = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield users_1.default.find();
        res.status(200).json({
            success: true,
            message: "All users found successfully",
            data: response
        });
    }
    catch (err) {
        console.log("Error in finding selected user: " + err.message);
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});
/* Get Selected User */
exports.GetSelectedUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield users_1.default.findById(req.params.id);
        res.status(200).json({
            success: true,
            message: "Selected user found!",
            data: response
        });
    }
    catch (err) {
        console.log("Error in finding selected user: " + err.message);
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});
/* Update User */
exports.UpdateUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield users_1.default.findByIdAndUpdate(req.params.id, {
            name: req.body.name,
            address: req.body.address,
            dob: req.body.dob
        });
        const getUser = yield users_1.default.findById(req.params.id);
        res.status(200).json({
            success: true,
            message: "User updated successfully!",
            data: getUser
        });
    }
    catch (err) {
        console.log("Error in updating selected user: " + err.message);
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});
/* Delete User */
exports.DeleteUser = (req, res, next) => __awaiter(void 0, void 0, void 0, function* () {
    try {
        const response = yield users_1.default.findByIdAndDelete(req.params.id);
        res.status(200).json({
            success: true,
            message: "User deleted successfully!",
            data: response
        });
    }
    catch (err) {
        console.log("Error in deleted selected user: " + err.message);
        res.status(400).json({
            success: false,
            message: err.message
        });
    }
});
//# sourceMappingURL=userController.js.map