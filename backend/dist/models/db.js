"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
// Connecting to the database
mongoose_1.default
    .connect("mongodb://localhost:27017/UserDetails", {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false
})
    .then(() => {
    console.log("Successfully connected to the database");
})
    .catch(err => {
    console.log("Could not connect to the database. Exiting now...", err);
    process.exit();
});
//# sourceMappingURL=db.js.map